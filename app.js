var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var assert = require('assert');
var config = require('./config.json');

var DB_NAME = config["database"];
var COLLECTION_NAME = config["collection"];
var ADDITIONAL_CONFIG = config["additionalConfigs"];

var DB_CONNECTION_STRING = config["connectionString"] + ADDITIONAL_CONFIG;
var JSON_ARRAY = require(config["json_data_file"]);
var BULK_STATEMENTS = require(config["bulk_statements_file"]);

var experiencedAndElearning = { $and: [{"statement.verb.display.en-GB": {$eq: "experienced"}, "statement.object.definition.type": {$eq: "http://cslearning.gov.uk/activities/elearning"}}] };
var notExperiencedAndElearning = { $and: [{"statement.verb.display.en-GB": {$ne: "experienced"}, "statement.object.definition.type": { $ne: "http://cslearning.gov.uk/activities/elearning"}}] };

var callback = (err, result, msg="completed") => {
    if (err) {
        console.log("Error: " + err)
        return
    }else if (result){
        console.log(msg);
    }
}

var insertDocument = async (db) =>{    
    var count = JSON_ARRAY.length;

    JSON_ARRAY.forEach(async (obj, i) => {
        var msg = "document [ " + (i+1) + " of " + count +" ] inserted";
        db.collection(COLLECTION_NAME).insertOne(
            obj,
            {"maxTimeMS": 5000}, 
            function(err, result){
                callback(err, result, msg);
            }
        );
    })
    return(0);

    // const BUCKET_SIZE = 1000;
    // console.log("number of documents: " + JSON_ARRAY.length );
    // const buckets = JSON_ARRAY.length / BUCKET_SIZE;
    // var start = 0;
    // var end = 0;

    // for (i=1; i<=buckets; i++){
    //     start = (i - 1) * BUCKET_SIZE;
    //     end = start + BUCKET_SIZE;
    //     var msg = "documents " + (start+1) + " - " + (end-1);
    //     jsonData = JSON_ARRAY.slice(start, end);

        // await db.collection(COLLECTION_NAME).insertMany(jsonData, 
        //     function(err, result) {
        //         callback(err, result, msg);
        //     }
        // );
    // }
    // return(0);
};

var removeDocuments = async (db) => {
    return await db.collection(COLLECTION_NAME).deleteMany(
        statementCriteriaTypeUrl)
};

var removeCollection = async (db) => {
    return await db.collection(COLLECTION_NAME).drop({});
};

var bulkWrite = async (db) => {

    // { deleteOne : {
    //     "filter" : {}
    // }},

    return await db.collection(COLLECTION_NAME).bulkWrite(BULK_STATEMENTS)
}

var updateDcument = async (db) => {
    return await db.collection(COLLECTION_NAME).update(
        notExperiencedAndElearning,
        { $set: { "ttl": -1 } }
    );
};

var updateDcuments = async (db) => {

    var dt = new Date();

    return await db.collection(COLLECTION_NAME).updateMany(

        // { "statement.actor.account.name": { $eq: "Kylee13@hotmail.com" }},

        notExperiencedAndElearning,
        { $set: { "ttl": -1 } }, {multi: true},
        // experiencedAndElearning,
        // { $set: { "ttl": 5 } }, {multi: true}
    );
};

var getDocumentsCount = async (db, query) => {
    await db.collection(COLLECTION_NAME).countDocuments(query, {"maxTimeMS": 600000},
        function(err, results) {
            assert.equal(err, null);
            if (err) return(1);

            console.log("found: " + results);
            return(0);
        }
    );
}


var createIndex = async (db, index) => {
    db.collection(COLLECTION_NAME).createIndex(index,
        function(err, results) {
            assert.equal(err, null);
            if (err) return(1);

            console.log("index has been created successfully");
        }
    );
    return(0);
}

var createTtlIndex = async (db) => {
    db.collection(COLLECTION_NAME).createIndex(
        // { "created_at": 1 }, 
        // { expireAfterSeconds: 5
        //   //,partialFilterExpression:{"statement.actor.account.name": "Myrna93@yahoo.com"}
        // },
        // { "ttl": 1  },
        // { "createdAt": 1 }, { expireAfterSeconds: 5 },
        // { "_ts": 1 },// { expireAfterSeconds: 0 },
        function(err, result) {
            callback(err, result, "index has been created");
        }
    );
    return(0);
};

var dropIndex = async (db, index) => {
    db.collection(COLLECTION_NAME).dropIndex(index,
        function(err, result) {
            callback(err, result, "index has been removed");
        }
    );
    return(0);
}

var reIndex = async (db) => {
    db.collection(COLLECTION_NAME).reIndex(
        function(err, results) {
            assert.equal(err, null);
            if (err) return(1);

            console.log("reindex successfull");
        }
    );
    return(0);
}

//ALL OPERATIONS ARE IN HERE
var doItAll = async (db, client) => {

    var iterations = 1;

    for(i=1; i<=iterations; i++){
        try{
            // console.log( "creating documents....");
            // var result = await insertDocument(db);
            
            // console.log( "updating documents....");
            // var result = await updateDcuments(db);

            // console.log( "creating index....");
            // var result = await createTtlIndex(db);

            console.log( "counting documents....");
            // var result = await getDocumentsCount(db, {});
            var result = await getDocumentsCount(db, experiencedAndElearning);
            // var result = await getDocumentsCount(db, notExperiencedAndElearning);
            // var result = await getDocumentsCount(db,{ "ttl" : 5, });

            // console.log("bulkWrite has started...");
            // var result = await bulkWrite(db);

            // console.log("deleting documents....");
            // var result = await removeDocuments(db);

            // console.log("removing the collection...");
            // var result = removeCollection(db);

            // console.log("re-indexing...");
            // var result = reIndex(db);

            // console.log("dropping index...");
            // var result = dropIndex(db, indexName);

        }catch(e){
            console.log("Error: "+ e);
        }finally{
            //client.close();
        }
    }
}

// starts here

MongoClient.connect(DB_CONNECTION_STRING,{useUnifiedTopology: true},function(err, client) {
    assert.equal(null, err);
    var db = client.db(DB_NAME);

    try{
        var result = doItAll(db, client);
    }catch(e){
        console.log(e);
    }finally{
        client.close();
    }
});



